# Lerndokumentation Modul 141

## Dokumentation über erlerntem aus Modul 141

#### beinhaltet Anleitungen, Theorie-Zusammenfassugen, Snagits

## Inhaltsverzeichnis

#### Dominik Gfeller
#### Technische Berufschule Zuerich
#### Klasse PE22e
#### dominik.gfeller@edu.tbz.ch
#### 22.02.2024 - 18.04.2024

## Einleitung

## Inhaltsverzeichnis

- [Einleitung](/modul_141/modulueberblick_m141.md#einleitung)
- [Aufträge](/modul_141/auftraege/)
    - [Tag-1](/modul_141/auftraege/tag-1/tag-1.md)
    - [Tag-2](/modul_141/auftraege/tag-2/tag-2.md)
    - [Tag-3](/modul_141/auftraege/tag-3/tag-3.md)
    - [Tag-4](/modul_141/auftraege/tag-4/tag-4.md)
    - [Tag-5](/modul_141/auftraege/tag-5/tag-5.md)
    - [Tag-6](/modul_141/auftraege/tag-6/tag-6.md)
    - [Tag-7](/modul_141/auftraege/tag-7/tag-7.md)
    
