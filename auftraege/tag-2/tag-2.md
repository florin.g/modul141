# M141 - DB-Systeme in Betrieb nehmen  (2.Tag)

## Vermerke von wichtigen Commands auffrischung
Alle SQL-Befehle lassen sich einer der vier Gruppen zuordnen:

* Geladene Sortierungen im CMD ausgeben: 
    show collation;

**Datenbanksprache SQL und Befehlsgruppen**                         
| SQL    | Datenbanksprache; Structured Query Language   |
|----------------------------|--------------------|
| **DDL** <br> (Data Definition  Language)         | DB-Objekte definieren, z.B.  <br> - CREATE db-objekt <br> - ALTER db-objekt <br> - DROP db-objekt |
| **DML** <br>(Data Manipulation  Language)   | Daten abfragen, einfügen, ändern löschen, z.B.: <br> - SELECT <br> - INSERT <br> - UPDATE <br> - DELETE                     |
| **DCL** <br> (Data Control  Language)            | Zugriffsrechte verwalten, z.B.   <br> - GRANT <br> - REVOKE                                     |
| **TCL** <br> (Transaction  Control  Language)    | <br> - TRANSACTION <br> - COMMIT <br> - ROLLBACK                                                                     |


### Passender SQL Befehl für eine vorgegebene Aufgabe:

| Tätigkeit                      | SQL-Befehl                                                            | Befehlsgruppe | Risiko des Befehls |
| ------------------------------ | --------------------------------------------------------------------- | ------------- | ------------------ |
| Alle Daten in Tabelle anzeigen | select \* from table_name;                                            | DML           | low                |
| Datenbank auswählen            | use database_name;                                                    | DDL           | low                |
| neue Datenbank erstellen       | create database database_name;                                        | DDL           | high               |
| neue  Tabelle erstellen        | create  table table_name;                                             | DDL           | high               |
| bestehende Tabelle löschen     | delete table table_name;                                              | DML           | high               |
| Tabellenstruktur kontrollieren | describe table_name;                                                  | DDL           | low                |
| Datenbank anzeigen             | show databases;                                                       | DDL           | low                |
| Tabellle in Datenbank anzeigen | show  tables;                                                         | DDL           | low                |
| Daten in Tabelle anzeigen      | select \* from  table_name                                            | DML           | low                |
| Daten in  Tabelle eintragen    | insert into table_name values (daten1, daten2);                       | DML           | medium             |
| Daten in Tabelle ändern        | Update table_name set colum1 = value1, colum2=value2 where condition; | DML           | medium             |
| Daten in Tabelle löschen       | delete  from table_name where condition;                              | DML           | high               |
| Spalte in Tabelle löschen      | alter table table_name drop collum colum_name;                        | DDL           | high               |

### Optionsdateinen vom eigenen System auslesen:
    C:\> mysqld --verbose --help

## Repetition der wichtigsten Zeichenkodierungen
-   **ASCII** (Veraltet, American Standard Code for Information Interchange): <br>   Ist 7-Bit codiert, hat ca. 32 Steuerzeichen und 96 druckbare Zeichen, ist auf die englische Sprache ausgerichtet.  
-   **ANSI** (ISO-8859): Der Zeichensatz [ISO-8859-1](https://de.wikipedia.org/wiki/ISO_8859-1) (**Latin1**) ist der erste von insgesamt 16 ländereigenen Zeichensätzen und ist für Westeuropa gemacht, beinhaltet also z.B. deutsche, französische oder skandinavische Sonderzeichen. ANSI deckt (fast) alle ASCII Zeichen (mit demselben Code) ab und codiert, mit dem höchstwertigen Bit (8.Bit) gesetzt (>=80h), weitere 96 internationale Zeichen via wählbare Codepages.
-   **Unicode** (Universal Character Set, UCS): Entstanden Ende der 80er Jahre mit dem Ziel alle Sprachen der Welt in einem Zeichensatz zu vereinen, ist der Unicode der grösste und umfassendste Zeichensatz. Durch die 32-Bit-Speichergrösse pro Zeichen kann Unicode ca. 100'000 verschiedene Zeichen einem eindeuigen Code zuordnen. 
- **UTF-8** (Standard, UCS Transformation Format 8-Bit): UTF-8 ist die häufigste verwendete Kodierung für Unicode-Zeichen mit hohem ASCII-Anteil – die ersten 128 Zeichen entsprechen der ASCII-Tabelle. Aktuell sind über 95% aller Websites mit UTF-8 codiert. Siehe [History](https://w3techs.com/technologies/history_overview/character_encoding) <br> Der Vorteil von UTF-8 liegt in der [angepassten Speichergrösse](https://de.wikipedia.org/wiki/UTF-8#Algorithmus) eines einzelenen Zeichens, das von einem Byte (ASCII) bis zu vier Bytes reicht (Emoji): in 8-Bit Sprüngen.
- **UTF-16, UTF-32**: Gegenüber UTF-8 ist die angepasste Speichergrösse bei UTF-16 16 Bit &#8594; Java), und bei UTF-32 fix 32 Bit, was einen grossen Speicherbedarf bei den häufigsten Zeichen (ASCII) bedeutet!

### Byte Order Mark -BOM
* steht ganz am Anfang der Datei
#### Big Endian oder auch little Endian
* Bei UTF-16 + UTF32 muss Reihenfolge von Bytes angegeben werden
    * FE FF = Big Endian 
    * FF FE = Little Endian
### MyISAM Tabellen
    * in drei Dateien abgespeichert
        * Damit ist es möglich ein Backup zu machen
    * Schneller als InnoDB

### InnoDB
    * Daten werden nur in einer Datei abgespeichert mit Beziehungen
    * Alle Table spaces sind in einer Datei (ibd)
        * Backup nur via MYSQL-Dump.exe
    * Garantiert Refferenzielle Integrität
        * ibdata1 enthält alle Daten von allen Datenbanken

## Soriterfolgen im Allgemeinen
### Defintion Sortierfolge: 
* Regelsatz für Vergleich von Zeichen in einem Zeichensatz
* Die Kombination von Buchstaben un ihrer Kodierungen bildet ein Zeichensatz

### Was MYSQL im bezug auf Sortierfolgen erlaubt. 
* Das Speicher vo Strings in Vielzahl von Zeichensätzhen
* Vergleichen von Strings mit einer Zeichenfolge
* Mischen von Strings verschiedenern Zeichensatze, Sortierfolgen auf selben Server, DB, Tabelle.
* Angabe von Zeichensatz und Sortierfolge (=Kollation)

## Kollation (Collation)
* definiet die sortierung innerhalb von Spalten
* unterschiedliche Sortierung des Alphabetes nach Gropss und Kleinschreibung
* repersentiert den Zeichensatz

### Standart Kollation: 
![](/modul_141/auftraege/tag-2/screenshots/standart_kollation.jpg)
*
####  Kolation ausgeben: 
    show collation;

#### Kollation welche hier verwendet werden: 
    utf8mb4_general_ci (Default), utf8mb4_unicode_ci, latin1_german1_ci oder latin1_german2_ci empfohlen
* Em Ende abzulesen, ob gross oder Kleinschreibung beachtet wird \
        *c*ase-*s*ensitive oder *c*ase-*i*nsensitive


## Übungen Die Datenbasis "Firma"

***

## SQL-Skripte(Batch Mode)
### Wichtige Befehle für Batch verarbeitung ausserhalb von Konsolenklient
    mysql -u root -p db < pfad\script.sql
-  Skript wo sich in Verzeichnis befindet einlesen und auf im commdn definierter DB ausführen

        mysql -u root -p db < pfad\script.sql > pfad\ausgabe.txt
* Skript ausführen und Ausgabe in einer Textdatei speichern

### Wichtige Befehle für Batch verarbeitung im Konsolenklient
    mysql> source pfad\script.sql

    mysql> \. pfad\script.sql


## Checkpoint beantwortung: 
![](../x_res/tbz_logo.png)


# ![](../x_res/CP.png) Checkpoint 2.Tag



## DB-Server und XAMPP

Bei den folgenden Fragen treffen eine oder mehrere Antworten zu.

1.  Wie kann der MySQL-Server gestartet werden?

    - [ ] Start von `mysql.exe` im CMD-Fenster

    - [x] Start von `mysqld.exe` im CMD-Fenster

    - [x] über MySQL-Workbench(fall als Dienst installiert ist)

    - [ ] Eingabe von localhost als URL im Browser

    - [x] `NET START mysql` (im CMD-Fenster)

    - [x] mit dem Dienstmanager von Windows(falls als Dienst installiert ist)

2.  Welche Informationen erhalten Sie, wenn Sie im Konsolenfenster den Befehl *status* eingeben?

    - [x] Version des Konsolenprogramms

    - [x] Betriebszeit des Servers

    - [x] Version des Servers

    - [ ] Betriebszeit des Monitors

3.  Welche Daten befinden sich im Verzeichnis datadir (z. B. C:…\\mysql\\data)?

    - [x] Protokoll-Dateien (Log-Files)

    - [x] Fehlerprotokolle

    - [ ] die ausführbaren MySQL-Programme, z.B. mysql.exe

    - [x] Datenbanken

4.  Wie prüfen Sie, ob der MySQL-Server läuft?

    - [x] mit dem Dienst-Manager von Windows(falls als Dienst installiert ist)

    - [x] mit dem GUI-Tool Administrator

    - [] durch Eingabe des Befehls `status` im CMD-Fenster

    - [] mit dem Task-Manager von Windows (Prozess)

5.  Wie testen Sie die Installation des DB-Servers?

* in das Verzeichnis C:\xampp\mysql\data wechseln, kontrollieren ob phpmyadmin, test, performance, mysql vorhanden sind
* Im Verzeichnis C:\xampp\mysql\bin cmd starten
* MYSQL -u root -p eingeben
    * fall nach Passwort gefragt, alles in Ordnung
* Statusabfrage des Server
* phpMyAdmin, Workbench-Admin-Bereich

6.  Wie überprüfen Sie die Laufzeit des DB-Servers?

    * status eingeben im Klient
    Parameter: UpTime; Info im phpMyAdmin; MySQL-Workbench

7.  Wozu verwenden Sie das Programm mysql.exe? Wie starten Sie es?

* Dies ist der MYSQL Cleint, dieser wird verwendet für die Interaktion mit dem MYSQL-Server
    MYSQL - u root -p 
    * in cmd eingeben im Verzeichnis C:\xampp\mysql\bin

8.  Notieren Sie 3 Informationen des status-Befehls mit ihrer Bedeutung.

* Uptime: Seit wann der Server aktiv ist
* Using delimiter: Welches Zeichen zum unterscheiden von Datensätzen verwendet wird oder commands
* Server characterset: Was für eine Kodierung der DB-Server verwendet
* Threads: Zeigt die Anzahl aktiven Threads an


9.  Nennen Sie 2 wichtige Verzeichnisse der MySQL-Installation mit ihrem Inhalt.

* C:\xampp\mysql\bin
    ausführbare Toll und DB-Programme

* C:\xampp\mysql\data
    beinhaltet MYSQL Datenbanken inklusive den Berechtigungen für den Zugriff auf die DB
    
10.  Was ist der Inhalt der `my.ini` – Datei?

* Konfigurationsparamter für MySQL-Server
    * Serverportnummer
    * Datenverzeichnis
    * max. Anzahl Verbindugen (Anzal Clients) auf den Server


## Codierung und Kollation

1.  Welche Aussagen treffen zum Thema Codierung zu?

    - [ ] Ein Datenbankserver erkennt die Codierung einer Datei automatisch

    - [x] Codierung ist eine Vereinbarung zwischen dem Nutzer und dem System.

    - [x] Die Codierung legt fest, welche binäre Bitkombination zu welchem Zeichen gehört.

    - [ ] ANSI- und ASCII-Codierung ist dasselbe

    - [x] Der Unicode-Zeichensatz hat 32 Bit Codelänge

    - [x] `UTF` bedeutet Unicode Transformation Format

    - [ ] `UTF-8 ` hat nur 8 Bit lange Zeichen aus dem Unicode-Zeichensat(Falsch da auch 16/32 Bit möglich)

2.  Welche Aussagen treffen zum Thema Byte Order Mark zu?

    - [] Ein BOM kann in Dateien jeglicher Art gesetzt werden.

    - [x] Wenn das UTF-8-BOM "ï»¿" bei einem Text-Editor sichtbar ist, erkennt er *es* nicht!

    - [x] Bei UTF-8 nutzt ein BOM nichts, da es nur 8 Bits zur Codierung verwendet! (Da Reihenfolge nicht verändert)

    - [x] UTF-8 und UTF-16 verwenden unterschiedliche BOMs.!

3.  Welche Aussagen treffen zum Thema Kollation zu?

    - [ ] ist die Standard-Einstellung bei MySQL.

    - [x] In der DIN-Normierung zur deutschen Kollation werden zwei Varianten zur Umlauthandhabung angeboten.

    - [ ] Die Endung "_ci" gibt an, dass die Sortierung die Gross-/Kleinschreibweise unterscheidet.

    - [x] Seit MySQL 5.5.3 sollte [utf8mb4](https://dev.mysql.com/doc/refman/5.5/en/charset-unicode-utf8mb4.html) anstelle von utf8 verwendet werden.

    - [x] In der Konfig-Datei (my.ini) kann die UFT8-Codierung als Standard angegeben werden.

    - [ ] Eine Kollationseinstellung gilt für die ganze Tabelle (Entität).


## Daten importieren

1.  Mit welchem Befehl kontrollieren Sie die Struktur einer Tabelle?

    - [ ] SHOW DATABASES;

    - [x] SHOW CREATE TABLE *tabellenname*;

    - [x] DESC *tabellenname*; 
    
    - [x] DESCRIBE *tabellenname*;

    - [ ] SELECT * FROM *tabellenname*;

    - [ ] SHOW TABLE *tabellenname*;



    