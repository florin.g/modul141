# DB-Systeme in Betrieb nehmen(1.Tag)

## Defintion und unterscheidung der Begriffe im Datenbankwesen
![](/modul_141/auftraege/tag-1/screenshots/database-architecutre.jpg)
* Datenbank (DB) strukturierte Sammlung von Daten

* DBS: Datenbank System oder DB-Server: Logische Einheit zur Verwaltung von Daten

* DBMS: Database Management System, Programm für die Verwaltung der Datenbank(en), DB-Engine

* Datenbasis: die eigentlichen Daten

#### Aufgaben von DB-System
* sichere Speicherung von Daten
* verarbeitung von Befehlen zu Abfrage der Daten
* Sortierung und Analyse vorhandener Daten
* Eintragen von neuen Daten
* ändern von bestehenden Daten
* löschen von Daten
### Notitzen vom Unterricht

* xampp nie auf pulbic server aufsetzen

* httpd.conf = hosttet die ganze Apache Webserver configuration

* mysqld.exe = sollte normalerweise

* mysql = sind sämtliche berechtigungen darin
    * es wäe nicht möglich seine DB zu starten


nosql = not only sql

4 gruppen von nicht realationalen datenbanken
* keyvalue
* graph
* 


MongoDB = mongolius-database = mehr offene strukturern
	* JSON und XML abzuspeichern
	Flexibler

Keyvalue
* Widecallum

Neo4j graph oriented

timeseries = 

* Wikipedia läuft unter MariaDB

## DB Server und DB-Client
* Datenbank-Server = Rechner die Dienstleistungen von Datenbanksystem zur verfügung stellen 
* Datenbank Client = Programme die mit Datenbanksysteme in Verbindung stehen 
    * Für Anwender: SChnittstelle zum Datenbankserver

### Datenbank-System-Architekturen
* mehre spezialisierte Servertypen in profesionlleer Apllikationsumgebung = Webserver, Netzwerkserver, Applikationserver


## Datenbank Modelle

| Art der Datenbank           | Aufbau                                                                                                                                                 | Beispiel                     | Verlinkung                                                            |
| --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------- | --------------------------------------------------------------------- |
| Hirarchische DB             | Daten in Baumstruktur organisert. Jede Ebene von Daten hat bestimmten Typ. Jeder Datensatz nur einen Besitzer.                                         | IMS (IBM)                    | https://www.ibm.com/docs/de/addi/5.0.2?topic=programs-imsdb-structure |
| Netzwerk DB                 | erweitert Hirarcisches Modell. Mehre Beziehngen pro Datensatz. Komplexe Beeziehungen zwischen verschiedenen Datentypen                                 | IDS (Integrated Datastore    |
| Relationale DB (RDBMS)      | Organisiert Daten in Tabellen oder Relationen. Bestehen aus Zeilen und Spalten. Jede Zeile = ein Datensatz, jede Spale eine Eigenschaft von Datensatz. | MYSQL, Oracle, MS-SQL-Server |
| Objektorientierte DB (OODB) | Infos in Form von Objekten gespeichert. Nützlich bei Anwendungen mit Komplexen Daten/Beziehungen                                                       | MONGODB                      |
| XML-DB                      | spezfisch für XML Dokumente                                                                                                                            | Tamino                       |                                                                       |
| NoSQL DB (not only SQL)     | Grpsse Datenmengen geeignet. Wenn unstruktuerirt + Datenstruktur über Zeit verändert gut                                                               | MongoDB                      |

### NOSQl Datenbank-Arten
#### Dokumentenorientierete Datenbank
* MongoDB, CouchDB, Cassandra
#### Grafdatebanken
* Neo4J
#### Big-Table Datenbanken
*  Doug Judd von Hypertable Inc., Big Table von Google
#### Key-Value-Datenbanken
*  Redis, Dynamo, Codmos


## Relationale DBMS: Vergleich MySQL vs MariaDB vs PostgreSQL

| Produkt                                                                    | MariaDB                                                                                                                                                                            | MySQL (Oracle) oder MS-SQL                                                                                                                      | PostgeSQL                                                                                                                                                                                    |
| -------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Merkmale                                                                   | Hochverfügbarkeit, Security, Interoperabilität, Prformanceverbesserungen im Vergleich zu MYSQL. Open Source RDBMS. Skalierbarer als MYSQL                                          | allgemein einsetzbares RDBMS                                                                                                                    | ORDBMS mit sehr vielen Features und prefferiert für read-write commands, grosse datensaätze und complexe queries                                                                             |
| Vorteile                                                                   | Unterstützt drei sekundäre Datenbankmodelle (Document Store, Graph DBMS, Spatial DBMS). Leicht zu migrieren von MYSQL. Hohe Abfrageperformance. Untersttütz alle MYSQL Koneektoren | dynamsiche Spalten + Datenmaskierung                                                                                                            | Gute Erweiterbarkeit + unstützung von komplexen Abfragen und Transaktionen, zuverlässigkeit, viele Features. Efficent backup and recovery features. Hsotere. More advanced stored procedures |
| Nachteile                                                                  | Seit Version(5.5.36) kein Fork von MYSQL und nicht. Keine JSON_TABLE unterstützung wie MariaDB                                                                                     | Nicht den selben standart betreffend Performance wie MariaDB. Keine JSON_Query+ JSON_EXISTS unterstützung wie MariaDB. Weniger Speicher Engines | Performance bei grossen Datenbanken eher eingeschränkt                                                                                                                                       |
| Anwendungsfälle                                                            | Webanwendungen, Content Management Systems. Verwaltung grosser Datenmengen                                                                                                         | Unternehmensanwendungen, E-Commerce, Websites,                                                                                                  | Data Warehousing, Geodatenbanken, complex webapplications                                                                                                                                    |
| https://aws.amazon.com/de/compare/the-difference-between-mariadb-vs-mysql/ |                                                                                                                                                                                    |                                                                                                                                                 |

| MySQL                                                                                     | MariaDB                                                                                                     |
| ----------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| MySQL speichert JSON-Berichte als binäre Objekte.                                         | MariaDB speichert JSON-Berichte in Zeichenketten. Der JSON-Datentyp von MariaDB ist ein Alias für LONGTEXT. |
| MySQL hat ein hohes Maß an Kompatibilität, unterstützt jedoch PL/SQL nicht.               | MariaDB hat ein hohes Maß an Kompatibilität und unterstützt PL/SQL seit Version 10.3.                       |
| MySQL ist bei der Replikation und Abfrage etwas langsamer als MariaDB.                    | MariaDB ist bei Replikation und Abfrage etwas schneller als MySQL.                                          |
| MySQL unterstützt Super-Schreibschutz-Funktionen, dynamische Spalten und Datenmaskierung. | MariaDB unterstützt unsichtbare Spalten und temporären Tabellenraum.                                        |
| MySQL hat die Komponente validate_password.                                               | MariaDB hat drei Passwort-Validator-Plugins.                                                                |
| MySQL-Datenbanken verwenden InnoDB und AES, um gespeicherte Daten zu verschlüsseln.       | MariaDB unterstützt temporäre Protokollverschlüsselung und binäre Protokollverschlüsselung.                 |
| MySQL hat weniger Speicher-Engines als MariaDB.                                           | MariaDB hat mehr Speicher-Engines als MySQL und kann mehrere Engines in einer Tabelle verwenden.            |
| MySQL hat zwei Versionen: MySQL Enterprise Edition und eine GPL-Version.                  | MariaDB steht vollständig unter GPL.                                                                        |
| MySQL verfügt in seiner Enterprise Edition über Thread-Pooling.                           | MariaDB kann über 200.000 Verbindungen gleichzeitig verwalten, was mehr als MySQL ist.                      |

## Andere DBMS

| Datenbankmodell | Produkt(e)                              | Vor-/Nachteile                                                                                                                                                                                                                                             | Typische Anwendungen                                                    | Bild, Bsp, Link, etc.                     |
| --------------- | --------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- | ----------------------------------------- |
| Document        | MongoDB, Couchbase                      | Vorteile: Flexibilität in der Datenstruktur, Skalierbarkeit, Einfachheit bei der Handhabung von semi-strukturierten Daten. Nachteile: Möglicher Overhead bei komplexen Transaktionen, Konsistenzherausforderungen.                                         | Content Management, Echtzeit-Analytik, Protokollierung von Ereignissen. | [Beispiel](https://www.mongodb.com/)      |
| Key-Value       | Redis, DynamoDB                         | Vorteile: Hohe Geschwindigkeit, einfache Skalierbarkeit, flexible Datenmodelle. Nachteile: Einschränkungen bei komplexen Abfragen, begrenzte Funktionalität für komplexe Datenmanipulationen.                                                              | Caching, Sitzungsmanagement, Echtzeit-Anwendungen.                      |                                           |
| Wide column     | Apache Cassandra, HBase                 | Vorteile: Hohe Skalierbarkeit, hohe Verfügbarkeit, flexible Schema-Änderungen. Nachteile: Komplexität bei der Datenmodellierung, begrenzte Unterstützung für komplexe Abfragen.                                                                            | Zeitreihenanalyse, Big Data, Logging.                                   | Apache Cassandra                          |
| Search          | Elasticsearch, Solr                     | Vorteile: Volltextsuche, schnelle Abfragen, Skalierbarkeit. Nachteile: Komplexität bei der Verwaltung von Indizes, begrenzte Unterstützung für komplexe Transaktionen.                                                                                     | E-Commerce-Suchfunktionen, Log-Analyse, Textanalyse.                    |                                           |
| Graph           | Neo4j, Amazon Neptune                   | Vorteile: Effiziente Darstellung und Abfrage von Beziehungen, flexible Datenmodelle, gut geeignet für soziale Netzwerke und Empfehlungssysteme. Nachteile: Skalierbarkeitsprobleme bei sehr großen Graphen, komplexe Abfrageoptimierung.                   | Soziale Netzwerke, Empfehlungssysteme, Wissensgraphen.                  | [Neo4j](https://neo4j.com/)               |
| Time Series     | InfluxDB, TimescaleDB                   | Vorteile: Optimiert für Zeitreihendaten, hohe Schreib- und Abfragegeschwindigkeiten, spezielle Funktionen für Zeitserienanalyse. Nachteile: Begrenzte Unterstützung für nicht-zeitbasierte Abfragen, Skalierbarkeitsprobleme bei sehr großen Datensätzen.  | Überwachungssysteme, IoT-Datenanalyse, Finanzdatenanalyse.              | [TimescaleDB](https://www.timescale.com/) |
| Spatial         | PostGIS, MongoDB (mit Geospatial-Index) | Vorteile: Unterstützung für Geodaten, spezielle Funktionen für räumliche Abfragen, Integration mit Geoinformationssystemen (GIS). Nachteile: Komplexität bei der Verwaltung von Geodaten, mögliche Leistungseinbußen bei komplexen räumlichen Operationen. | Kartendienste, Standortanalysen, Geofencing.                            | [PostGIS](https://postgis.net/)           |


## wichitge vermerke zur Inbetriebnahme von Datenbankserver: 
### PFAD vom Datenbank-Management-System
    C:\xampp\mysql\bin

### Starten des Server via der Kommandozeile: 
* cd ins Verzeichnis: C:\xampp\mysql\bin
* anschliessend: mysql -u root -p (cmd)
    * oder: .\mysql -u root -p (powershell)

### Importieren eines SQL Files: 
    mysql -u username -p database_name < file.sql

### löschen einer Tabelle aus einer Datenbank: 
    DROP TABLE tabellenname;

### Outputs Formatieren;
#### Ausgabe in vertiakel statt in horizontal (Hilfreich bei grossen Tabellen)
* \G am ende des Statemnts schreiben, ; ersetzen durch \G
##### Normale Ausgabe (horizontal (;)): 
![](/modul_141/auftraege/tag-1/screenshots/sql_ausgabe_horizontal.jpg)
##### Ausgabe in vertikal (\G)
![](/modul_141/auftraege/tag-1/screenshots/sql_ausgabe_vertikal.jpg)

### löschen eines Indexes:
    ALTER TABLE table_name DROP INDEX name-of-the-index;

### Leeren der SQL Tabelle, alle daten in einer in einer Tabelle entfernen.
    SQL: truncate table_name

### Tabelle um eine Attribut erweitern: 
    ALTER TABLE tbl_mitarbeiter
    ADD FS_Wohnort INT DEFAULT 0;

### Daten einfügen in SQL
    INSERT INTO tbl_mitarbeiter (Name, FS_Wohnort)
    VALUES ('Mitarbeiter1', 1), ('Mitarbeiter2', 2);

### Leeren einer Tabelle in SQL
    TRUNCATE TABLE db-name.table-name;

### erweitern von Tabelle mit einem Fremdschlüssel

### Abfragen ob eine gewisse Bedingung in einer Tabelle erfüllt ist + alle Spalten ausgeben
    select * from tbl_mitarbeiter where FS_Wohnort != ('NULL')


## Server Status abfragen
    status;
![](/modul_141/auftraege/tag-1/screenshots/sql_status-ausgabe.jpg)

### erklärung der verschiedenen Parameter: 
| Status-Informationen |                                                     |
| -------------------- | --------------------------------------------------- |
| Current database     | Aktuell gewählte Datenbank, z.B. mybooks            |
| Current user         | User- und Hostname (admin: root@localhost)          |
| Server Version       | Version der Server-Software,                        |
| Server characterset  | Zeichensatz des DB-Servers, z.B. utf8mb4            |
| TCP port             | vom Server verwendetes Port (Default: 3306)         |
| Uptime               | Betriebszeit seit dem letzten Serverstart           |
| Server-Informationen |                                                     |
| Host Name            | Name des Server-Rechners,                           |
| OS Plattform         | Betriebssystem des Server-Rechners, z.B. Windows NT |
| Local IP Address     | Netzwerkadresse des Server-Rechners, z.B. 127.0.0.1 |
| Server Info          | Version des Servers mysqld, z.B. 4.11.10            |


## Checkpoint beantwortung
![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen


# ![](../x_res/CP.png) Checkpoint 1.Tag


# Einführung, DB-Engines, XAMPP, Workbench

Bei den folgenden Fragen treffen eine oder mehrere Antworten zu.

1.  Welches ist die heute am **häufigsten** verwendete Datenbank-Art?

    - [ ] Hierarchische Datenbank

    - [x] Relationale Datenbank

    - [ ] Objektorientierte Datenbank

    - [ ] Netzwerkförmige Datenbank

2.  Welche **Komponenten** sind in einem DB-Server enthalten?

    - [x] 1 oder mehrere Datenbanken

    - [ ] 1 oder mehrere Datenbank-Anwendungen

    - [x] Datenbank-Management-System (DBMS)

    - [ ] Formulare, Reports und Abfragen

3.  Bei welchen der folgenden **Fabrikate** handelt es sich um eine relationale Datenbank?

    - [x] Oracle

    - [ ] Couch-DB

    - [x]  MySQL

    - [x] MariaDB

    - [ ] Mongo-DB

    - [x] MS Access

4.  Welches sind Beispiele für **Aufgaben** eines DB-Clients?

    - [] speichert die eigentlichen Daten

    - [x] stellt dem Benutzer ein User-Interface für den Datenzugriff zur Verfügung

    - [ ] verwaltet Benutzer und Passworte und gewährleistet damit die Sicherheit der Datenbank

    - [x] leitet die Befehle des Benutzers an den DB-Server weiter

5.  Welches sind **Client-Komponenten** von MySQL?

    - [] mysqld

    - [ ] my.ini

    - [x] mysql

    - [x] phpMyAdmin
    
6.  Wie heisst die **Server-Komponente** von MySQL?

    - [ ] phpMyAdmin

    - [ ] Workbench °

    - [ ] mysql

    - [x] mysqld

7.  Beschreiben Sie den Begriff Client/Server-Modell.

    * Die Aufgaben und Dienstleistungen wereden innerhalb eines Netzwerkes auf dedizierte Servertypen (Datenbank-Server, Webserver, etc.) verteilt
    * Aufgaben und Dineste werden zwischen Server und Clients aufgeteilt. 
    * Der Server verwaltet die effektiven Daten im Falle von einer DB
    * Die Clients stellen Anfragen an den Server und erhalten vom Server antworten auf die gestellten Anfragen
    * Dies hilft bei der Skalierung und nutzung der Resssourcen
       
8.  Welche Vorteile hat die Client/Server-Architektur gegenüber einer Desktop-DB?

    * Daten können bei einer Client/Server-Architektur zentral verwaltet werden. 
    * Von verschiedenen Clients(Standorten) kann die DB verwaltet werden.
    * Sicherhheit kann verbessert werden

    * (Entfernter Zugriff über Netz möglich
    * Multiuserfähig)
      
9.  Wie werden die Daten in einer relationalen Datenbank abgespeichert?

    * Daten werden in Spalten und Zeilen (Tabellen) gespeichert, welche in gewissen Verhältnissen zueinander stehen.
    * Die Beziehungen werden mit Primär und Fremdschlüssel definiert.
    * dritter Normalform
    * Nicht redundant, ref. Integrität
      
10.  Was sind die Vorteile, wenn ein DB-Server die **referentielle Datenintegrität** unterstützt?

    *  damit wird sichergestellt, dass es keine ungültigen Beziehungen zwischen Tabellen gibt.
    *  Alle Werte aller Fremdschlüssel müssen gültig sein!
    * verhindert inkonsistente Datensätze (Widerspruchsfreiheit der Daten & Beziehungen)

11.  Welches sind die 4 Gruppen von **NoSQL**-Datenbanken, die zurzeit relevant sind?

    * Dokumentenorientierete Datenbank
        * MongoDB, CouchDB, Cassandra
    *  Grafdatebanken
        * Neo4J
    *  Big-Table Datenbanken
        *  Doug Judd von Hypertable Inc., Big Table von Google
    * Key-Value-Datenbanken
        *  Redis, Dynamo, Codmos
    
12.  Was bedeutet **DBaaS**? Erklären Sie anhand eines Beispiels.

    * Database as a Service
    * Datenbank services in der Cloud, welche mietbar sind. 
    * Der Cloud-Anbieter (AWS, Azure) stellt eine ganze Datenbankumgebung bereit
    * Die darunterliegende Infrastruktur ist nicht relevant für den Betreiber, der Cloud Anbieter kümmert sich darum.
    * Cloud DB-Service zugeriff ohne installationen zu tätigen

13.  Was sind die Vorteile eines RDBMS gegenüber anderen DB-Modellen?

    * Daten liegen strukturiert an und sind so leicheter abfragbar, sowie auch auszuwerten
    * Flexibilität
    * Zusammenarbeit
    * redundanzfrei und konsistente Datenspeicherung
    * komplexe Abfragen der Daten möglich
    * Standartisiert (genormt) = Datenaustausch erleichtert
    * Skalierbar
    * Datenintegrität und konsistenz
    
14.  DB-Server starten und stoppen

    Stoppen und starten Sie Ihren DB-Server auf die verschiedenen Arten. Kontrollieren Sie jeweils das Resultat mit dem Task-Manager:

	-   Über das XAMPP – Control-Panel
	-   Via Konsole (CMD / WPS)
	-   Über die MySQL-Workbench (nur wenn der als Service läuft) 
	
15. DB-Server prüfen

    Kontrollieren Sie, ob der MySQL-Server läuft mit dem

	-   Task-Manager von Windows: dort sollte ein Prozess mysqld.exe laufen.
	-   Dienst-Manager von Windows: der Dienst MySql sollte den Status gestartet haben.
	-   Mit den drei Klienten mysql, Workbench und phpMyAdmin 


