# M141 - DB-Systeme in Betrieb nehmen  (3.Tag)

## Allgemeine vermerke über den Tag



## Transaktionen in MYSQL 

### Starten einer Transaktion 
    Begin
* anschliessend kann man commands Absetzen 
    * Wenn Beginn gestartet, alles gesperrt. 
        * ein Client wartet, dass der andere
    * geschützter Raum
        * zwei bedingugen zum abbrechen: 

    Rollback
    * alles geänderte zurücksetzen

    Commit
    * alle änderungen übernehmen

- MyISAM hat die ganze Tabelle gesperrt

- Acid
    * Relevant für NOSQL


## Atomarität
* entweder alle  Befehle durchgefürht oder keiner durchgeführt

## Konsistenz
* 

## Isoliertheit
* verschiedene User sehen nicht was der Gegeüber tut

## Dauerhaftigkeit

***

## Tabellentypen und Transaktionen

### Feature Histoire in MYSQL
* bis vor 2000 keine Refferenzielle Integrität in MYSQL vorhanden
* Seit Mai 2000: Tabellentyp BDB (Berkley Database – Key/Value-DB) integriert
* MyISAM keine Transaktionsunterstützung 

### Tabellentypen
* Verwendung Transaktion und refferneitelle integrität(check Richtigkeit von Relation) abhängig von Tabellentyp

#### Unterschiede von MYISAM UND INNODB 
* InnoDB ist der standart Tabellentyp!

* Beschreibung ISAM Tabelle in .FRM Datei gespeichert wo in Verzeichnis von DB-Namen entspricht

* InnoDB-Tabelle verwendet Tablespace (art virtueller Speicher) für alle Tabellen und Indizes. 

![](/modul_141/auftraege/tag-3/screenshots/Unterschiede_myISAM_und_InnnoDB.jpg)


### Tabellenformat InnoDB und die Tablespace

#### InnoDB-Tabelle erzeugen
![](/modul_141/auftraege/tag-3/screenshots/erstellung_innodb-tabelle.jpg)

##### ändern von Tabellentypen:
    mysql> ALTER TABLE tblname ENGINE = InnoDB;

### Hotel Datenbank Übung:
#### Ändern von tabelle benutzer zu InnoDB

#### Typ(Format) jeder Tabelle anzeigen
    SHOW TABLE STATUS;
![](/modul_141/auftraege/tag-3/screenshots/typ_jeder_Tabelle_hotel-db_ausgeben.jpg)

#### Kontrolliere ob Tabelle benutzer Format InnoDB hat
    SHOW TABLE STATUS LIKE 'benutzer';
![](/modul_141/auftraege/tag-3/screenshots/typ_der_Tabelle_benutzer_ausgeben-hotel-db.jpg)

### Tablespace
* Bei start von MYSQL
    - Datei ibdata1 mit einer Grösse von 10 MByte (oder 12MB je nach Version) erzeugt
    - Sobald mehr Platz benötigt
        - in 8 MB Schritten vergrössert
    - ib_logfile0, -1 und ib_arch_log_000000000 erstellt

![](/modul_141/auftraege/tag-3/screenshots/table_space-architecturejpg.jpg)

#### Feststellen von freiem Speicherplatz von Tablespace
    SELECT SPACE,NAME,ROUND((ALLOCATED_SIZE/1024/1024), 2) as "Tablespace Size (MB)"  FROM information_schema.INNODB_SYS_TABLESPACES ORDER BY 3 DESC;


*** 

## Transaktionen
### Defintion von Transaktionen
* mehrere SQL-Commands in einem sicheren Block eingekapselt werden
    - erst wenn Commited wird, werden die änderungen übernommen.
* Dient der sicheren Ausführung und erhöht Sicherung und Konsistenz der Daten
    * Erleicherter Programmierung, effizienz
* Alle Kommandos zwischen beginn und comit werden entweder vollständig oder gar nicht ausgeführt.
    - (1) Transaktionen stellen sicher, dass die Daten nicht quasi gleichzeitig von anderen Anwendern verändert werden.

#### Gefahr bei nicht verwendung von Transaktionen
    Wenn eine Änderungen Fremdschlüssel betrifft, dann könnte es sein, dass ein andersweitiger geänderter Fremdschlüssel auf einen unterdessen gelöschten Datensatz verweist! (=> Inkonsistenz!)

### SQl Transaktions-Kommandos
    BEGIN (oder START TRANSACTION): Starten Transaktion
    COMMIT;: führt alle seit begin abgesetzen Kommandos aus

### Der Autocommit-Modus
* Alle SQL-Kommandos die nicht als Transaktion vermerkt wurden sofort ausgeführt
    - AUTOCOMMIT=1

* Zu deaktivieren: 
    SET AUTOCOMMIT=0

* InnoDB kommt gut langen Transaktionen zurecht


## Locking
### Schutz ohne Transaktion 
    LOCK-Kommanda verwenden
* Kurzzeitig ganze Tabelle für Alle Clients blockiert
    - äussersts ineffizient

### Schutz mit Transaktionstaugliche
* Nur Datensätze blockieert, die tatsächlich verändert werden

* InnoDB- und Gemini-Tabellen werden die Datensätze automatisch gesperrt

* erkennen automatisch Deadlock-Situationen, bei denen einzelne Prozesse endlos aufeinander warten-
    * automatisch Rollback durchgeführt

* InnoDB als auch Gemini
    - Crash Recovery
        - Allle Tabellen beim nächsten STart nach Stromausfall automatisch wiederhergestellt. 
    

| Locking-Mechanismen |                                    |
|---------------------|------------------------------------|
| MyISAM              | **Table**-Locking                      |
| BDB                 | **Page-Level**-Locking - Speicherseite |
| Gemini              | **Row-Level**-Locking – Datensatz      |
| InnoDB              | **Row-Level**-Locking - Datensatz      |



### Locking-Verhalten von InnoDB
#### Auto locking
* InnoDB selbst kümmert sich um das Locking
* Lock Table nicht ausführen!!
* betroffene Datensätze können nicht geändert werden.
    - Bessonderheit: selct Kommandos könne trotz gesperrter Datensätze ausgeführt werdenn.
        - andere Clienten igonieren änderungen von Transaktion so lange bis commited 

#### Manuelles Locking
* explizietes Sperren von Tabellen
* emulieren von Transaktionen
* höhere Geschwindikgeit bei aktualierung

    LOCK TABLES
    UNLOCK-TABLES

##### Funktion von LOCK TABLES sicherstellen
* Systemvariable innodb_table_locks auf 1 (Standard) und autocommit auf 0 (1 ist Standard) gesetzt. 
    - Falls falsch konfiguriert, keine Fehlermeldung ausgegeben.

##### SELECT ...FOR UPDATE Kommando

    BEGIN
    SELECT * FROM table WHERE x>10 FOR UPDATE
    -- die ausgewählten Datensätze
    -- sind jetzt exklusiv gesperrt
    COMMIT; -- oder ROLLBACK;

* Datensätze auch ohne Veränderung sperren

##### SELECT ... LOCK IN SHARE MODE Kommando
* Darauf wrten bis alle offen transaktonen abgeschlossen sind
    * select wartet bis exclusive locks auf Datensatz aufgelöst sind.

* Von select ausgegebnen Datnsätze mit shared loc gesperrt
    * Daten immernoch lesbar von anderen Usern

    BEGIN

    SELECT * FROM table WHERE x>10 LOCK IN SHARE MODE
    -- die ausgewählten Datensätze sind
    -- jetzt durch shared locks gesperrt

    COMMIT; -- oder ROLLBACK;


*   eingesetzt werden, wenn Datensätze zwar selbst nicht verändern, aber sicher sein, dass die Daten auch von anderen Anwendern nicht geändert werden.


## ACID / AKID
### Definiton ACID
Atomarität (atomicity), Konsistenz (consistency), Isoliertheit (isolation) und Dauerhaftigkeit (durability

#### Atomarität
* Block wird entweder ganz oder gar nicht ausgeführt

#### Konsistenz
* Wiederspruchfreiheit der Daten gewährleistet

#### Isoliertheit
* verschiedene Datenbankmanipulationen dürfen sich gegenseitig beinflussen (Sperrkonzept)

#### Dauerhauftigkeit
* Nach Abschluss Transaktion Veränderungen Dauerhaft gespeichert werden












![](../x_res/tbz_logo.png)

# M141 - DB-Systeme in Betrieb nehmen


# ![](../x_res/CP.png) Checkpoint 3.Tag


## Tabellentypen und Transaktionen
1.  Wie bezeichnet man die Ausführung mehrerer DB-Operationen in einem einzigen Schritt?

    - [ ] Referentielle Integrität

    - [ ] Replikation

    - [x] Transaktion

    - [ ] Storage Procedure

2.  Warum sollen Locks möglichst schnell freigegeben werden?

    - [] damit das DBMS nicht zu stark belastet wird

    - [x] damit andere DB-Anwender nicht lange warten müssen

    - [ ] damit niemand die Daten ändern kann

    - [] damit möglichst viele Benutzer gleichzeitig auf die DB zugreifen können

3.  Welches ist das Standard-Tabellenformat von MySQL (MariaDB)?

    - [x] InnoDB

    - [ ] MyISAM

    - [ ] ARIA

    - [ ] ISAM

4.  Wann verwenden Sie das InnoDB-Tabellenformat?

    - [ ] wenn möglichst schnell auf die Daten zugegriffen werden muss

    - [ ] wenn auf gar keinen Fall ein Datenverlust vorkommen darf

    - [x] wenn viele Benutzer gleichzeitig Daten ändern

    - [ ] wenn bei sehr vielen Daten nicht beliebig viel Speicherplatz vorhanden ist

5.  Was trifft auf den sog. Tablespace zu?

    - [ ] Datei, welche die Daten der entsprechenden Tabelle enthält (\*.MYD)

    - [ ] Datei, welche Beschreibung, Daten und Indexe einer Tabelle enthält

    - [x] Datei, welche alle InnoDB-Tabellen enthält (virtueller Speicher)

    - [x] wird nach Erreichen von x MB automatisch vergrössert (falls autoextend eingeschaltet)
    
6.  Mit welchen Befehlen werden Transaktionen gesteuert?

    - [ ] UNLOCK TABLES;

    - [x] COMMIT; oder ROLLBACK;

    - [ ] ALTER TABLE ... TYPE= ...;

    - [x] BEGIN; oder START TRANSACTION;

7.  Was trifft auf das Locking bei Transaktionen auf InnoDB-Tabellen zu?

    - [ ] in Transaktionen kommt Table locking zur Anwendung

    - [x] es wird Row locking angewendet

    - [ ] es werden alle Datensätze der entsprechenden Tabelle(n) gesperrt

    - [x] es werden nur die gerade bearbeiteten Datensätze gesperrt

8.  Welches sind Vorteile der InnoDB-Tabellen gegenüber MyISAM-Tabellen?

    - Unterstützung von Transaktionen
    - Row Level Locking 
    - Bessere Wiederherstellungsmechanismen
    - Bessere Fremdschlüsselbeziehungen 
    - unterstützung von reffernetiellen Integrität
      

9.  In welchen Dateien wird die MyISAM-Tabelle KUNDEN gespeichert?

    * KUNDEN.MYD(DATEN)
    * KUNDEN.MYI(Indizes)
    * KUNDEN.FRM(Tabellenstruktur)
      

10.  Notieren Sie den SQL-Befehl, der die InnoDB-Tabelle BESTELLUNGEN erstellt.

    CREATE TABLE BESTELLUNGEN (
    ...
) ENGINE=InnoDB;
    
      

11.  Welche Locking-Art ist a) bei MyISAM-Tabellen b) bei InnoDB-Tabellen möglich?

    a.) Table-level locking
    b.) Row Level Locking +1 Table-level locking  
      

12.  Beschreiben Sie den Begriff Datenbank-Transaktion!

    * gruppieren von meheren SQL-Befehlen
    * Mehre SQL Befehle zu einer logischen Einheit verpacken und dann entweder komplett oder gar nicht ausführen.
    * Transaktion sperrt die angewählten DS und arbeitet auf Kopie der DB-Tabelle
      

13.  Beschreiben Sie die Bedeutung von I in der Abkürzung ACID.

    * Isolation (Isoliertheit)
    * 
      

14.  Wie stellen Transaktionen bei einem DB-Server-Crash die Datenkonsistenz sicher? (Schwierig)

    * Die Transaktion wird als nicht ausgeführt und anschliessend sobald der DB-Server wieder läuft erneut ausgeführt.
        * In der Transaktionslog sind Details zum Status festgehalten. (Siehe auch Tag 6 Data Recovering.)  
  
15. Mit welcher Locking-Art wartet ein SELECT-Befehl, bis alle Transaktionen auf die angeforderte Tabelle entsperrt sind? 

    * select ... lock in share mode
    * Shared lock= nicht ganz so stark wie exclusive lock
    * Daten von anderen Anwendern ebenfalls nicht verändert werden
        * Daten lassen sich mit Select...Lock in Share Mode lesen  
          
16. Wie muss Autocommit gesetzt werden, damit jeder SQL-Befehl zu einer Transaktion gehört und damit explizit mit COMMIT abgeschlossen werden muss, damit er ausgeführt wird? 

    SET AUTOCOMMIT=0;
    * dies deaktivert den Autocommit Modus
